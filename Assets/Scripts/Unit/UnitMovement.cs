﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(UnitBase)), RequireComponent(typeof(UnitAttack))]
public class UnitMovement : MonoBehaviour
{
  [SerializeField]
  private float speed;

  private UnitBase unitBase;
  private UnitAttack unitAttack;

  public void Initialize()
  {
    unitBase = GetComponent<UnitBase>();
    unitAttack = GetComponent<UnitAttack>();
  }

  public void Step()
  {
    if (transform.position == unitBase.position)
    {
      GameObject target = unitAttack.GetTarget();
      if (target)
      {
        Stack<Point> path = AStar.Pathfind(unitBase.position, target.GetComponent<UnitBase>().position);
        if (path.Count != 0 && !ManagerUnit.Instance.IsOccupied(path.Peek()))
          unitBase.position = path.Peek();
      }
    }
    else
    {
      transform.position = Vector2.MoveTowards(transform.position, unitBase.position, speed);
    }
  }
}
