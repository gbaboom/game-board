﻿using UnityEngine;

public class UnitStatus : MonoBehaviour
{
  [SerializeField]
  private int health;
  private int currentHealth;
  public float healthPercent { get { return currentHealth / health; } }

  [SerializeField]
  private int regeneration;

  public void Initialize()
  {
    currentHealth = health;
  }

  public void Step()
  {
    currentHealth += regeneration;

    if (currentHealth <= 0)
      ManagerUnit.Instance.Remove(gameObject);
  }

  public void UpdateHealth(int value)
  {
    currentHealth += value;

    if (currentHealth > health)
      currentHealth = health;
  }
}
