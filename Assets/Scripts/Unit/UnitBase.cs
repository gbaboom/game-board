﻿using UnityEngine;

public class UnitBase : MonoBehaviour
{
  [SerializeField]
  private int _cost;
  public int cost
  {
    get
    {
      return _cost;
    }
  }

  public EnumOwner owner { get; private set; }

  private Point _position;
  public Point position
  {
    get
    {
      return _position;
    }
    set
    {
      ManagerUnit.Instance.UpdateOccupied(_position, value);
      _position = value;
    }
  }

  public void Load(Point position, EnumOwner owner)
  {
    this.position = position;
    this.owner = owner;
  }

  public void Initialize()
  {
    transform.position = position;
    transform.SetParent(ManagerUnit.Instance.holder);
    switch (owner)
    {
      case EnumOwner.Player:
        GetComponent<SpriteRenderer>().color = new Color32(0x29, 0x62, 0xFF, 0xFF);
        break;
      case EnumOwner.Computer:
        GetComponent<SpriteRenderer>().color = new Color32(0xB7, 0x1C, 0x1C, 0xFF);
        break;
    }
  }
}
