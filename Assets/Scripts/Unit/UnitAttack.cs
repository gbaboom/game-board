﻿using UnityEngine;

[RequireComponent(typeof(UnitBase))]
public class UnitAttack : MonoBehaviour
{
  enum Count
  {
    Single,
    All
  }

  [SerializeField]
  private EnumTarget _target;
  private EnumOwner target;

  [SerializeField]
  private Count count;

  [SerializeField]
  private int value;

  [SerializeField]
  private int range;

  [SerializeField]
  private int splash;

  private UnitBase unitBase;

  public void Initialize()
  {
    unitBase = GetComponent<UnitBase>();
    target = unitBase.owner.GetTarget(_target);
  }

  public void Step()
  {
    if (count == Count.Single)
      HandleSplash(ManagerUnit.Instance.GetClosest(unitBase.position, target), target);
    else
      foreach (var unit in ManagerUnit.Instance.GetAllInRange(unitBase.position, target, range))
        HandleSplash(unit, target);
  }

  public GameObject GetTarget()
  {
    GameObject unit = null;

    if (value < 0)
      // Closest enemy to attack
      unit = ManagerUnit.Instance.GetClosest(unitBase.position, target);
    else
      // Least health friendly to heal
      unit = ManagerUnit.Instance.GetLeastHealth(unitBase.position, target);

    // Out of range, return as move target
    if (unit && Point.Distance(unit.GetComponent<UnitBase>().position, unitBase.position) > range)
      return unit;

    return null;
  }

  private void HandleSplash(GameObject unit, EnumOwner owner)
  {
    if (!unit)
      return;

    // Out of range
    UnitBase unitBase = GetComponent<UnitBase>();
    if (Point.Distance(unit.GetComponent<UnitBase>().position, unitBase.position) > range)
      return;

    // Splash
    if (splash == 0)
      UpdateHealth(unit);
    else
      foreach (var other in ManagerUnit.Instance.GetAllInRange(unit.GetComponent<UnitBase>().position, owner, splash))
        UpdateHealth(other);
  }

  private void UpdateHealth(GameObject unit)
  {
    UnitStatus otherStatus = unit.GetComponent<UnitStatus>();
    if (otherStatus)
      otherStatus.UpdateHealth(value);
  }
}
