﻿using System.Collections.Generic;

public static class AStar
{
  private struct Node
  {
    public Point position;
    public float score;

    public Node(Point position, float score)
    {
      this.position = position;
      this.score = score;
    }
  }

  private class BinaryHeap
  {
    private List<Node> heap;

    public BinaryHeap()
    {
      heap = new List<Node>();
      heap.Add(new Node());
    }

    public bool Empty()
    {
      return heap.Count <= 1;
    }

    public void Clear()
    {
      heap.Clear();
      heap.Add(new Node());
    }

    public void Push(Node node)
    {
      heap.Add(node);

      // Bubble up
      int idx = heap.Count - 1;
      while (idx > 1 && heap[idx].score < heap[idx / 2].score)
      {
        Node temp = heap[idx];
        heap[idx] = heap[idx / 2];
        heap[idx / 2] = temp;
        idx /= 2;
      }
    }

    public Node Pop()
    {
      Node result = heap[1];
      heap[1] = heap[heap.Count - 1];
      heap.RemoveAt(heap.Count - 1);

      // Sink Down
      int idx = 1;
      while (idx * 2 < heap.Count)
      {
        // Get lower value child
        int left = idx * 2;
        int right = left + 1;
        int target = (right < heap.Count && heap[right].score < heap[left].score) ? right : left;

        // Stop sinking
        if (heap[target].score >= heap[idx].score)
          break;

        Node temp = heap[idx];
        heap[idx] = heap[target];
        heap[target] = temp;
        idx = target;
      }

      return result;
    }
  }

  public static Stack<Point> Pathfind(Point start, Point end)
  {
    BinaryHeap openList = new BinaryHeap();
    Dictionary<Point, Node> closedList = new Dictionary<Point, Node>();
    openList.Push(new Node(start, Point.Distance(start, end)));
    closedList.Add(start, new Node(start, 0.0f));

    while (!openList.Empty())
    {
      Node node = openList.Pop();
      Point[] neighbours = new Point[4];
      neighbours[0] = node.position + new Point(1, 0);
      neighbours[1] = node.position + new Point(-1, 0);
      neighbours[2] = node.position + new Point(0, 1);
      neighbours[3] = node.position + new Point(0, -1);
      foreach (var neighbour in neighbours)
      {
        // Reached end
        if (neighbour == end)
        {
          Stack<Point> path = new Stack<Point>();
          path.Push(neighbour);
          Point current = node.position;
          while (current != start)
          {
            path.Push(current);
            current = closedList[current].position;
          }
          return path;
        }

        // A-Star
        if (ManagerTile.Instance.IsWalkable(neighbour))
        {
          float score = closedList[node.position].score + 1;
          if (!closedList.ContainsKey(neighbour) || score < closedList[neighbour].score)
          {
            openList.Push(new Node(neighbour, score + Point.Distance(neighbour, end)));
            closedList[neighbour] = new Node(node.position, score);
          }
        }
      }
    }

    // No path found
    return new Stack<Point>();
  }
}
