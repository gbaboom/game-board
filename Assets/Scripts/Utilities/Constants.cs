﻿public enum EnumTarget
{
  Friendly,
  Enemy
}

public enum EnumOwner
{
  Player,
  Computer
};

public static class EnumExtensions
{
  public static EnumOwner Other(this EnumOwner owner)
  {
    return owner == EnumOwner.Player ? EnumOwner.Computer : EnumOwner.Player;
  }

  public static EnumOwner GetTarget(this EnumOwner owner, EnumTarget target)
  {
    return target == EnumTarget.Friendly ? owner : owner.Other();
  }
}
