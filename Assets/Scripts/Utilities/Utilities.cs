﻿using System;

public class Utilities
{
  static public int[,] ParseLevel(string str)
  {
    string[] level = str.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
    int[,] array = new int[level[0].Length, level.Length];
    for (int y = 0; y < array.GetLength(1); ++y)
      for (int x = 0; x < array.GetLength(0); ++x)
        array[x, array.GetLength(1) - 1 - y] = (int)char.GetNumericValue(level[y][x]);
    return array;
  }
}
