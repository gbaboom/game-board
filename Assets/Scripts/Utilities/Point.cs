﻿using UnityEngine;
using System;

public struct Point
{
  public int x;
  public int y;

  public Point(int x, int y)
  {
    this.x = x;
    this.y = y;
  }

  public static implicit operator Vector2(Point point)
  {
    return new Vector2(point.x, point.y);
  }

  public static implicit operator Vector3(Point point)
  {
    return new Vector3(point.x, point.y, 0);
  }

  public static bool operator ==(Point lhs, Point rhs)
  {
    return lhs.x == rhs.x && lhs.y == rhs.y;
  }

  public static bool operator !=(Point lhs, Point rhs)
  {
    return !(lhs == rhs);
  }

  public override bool Equals(object obj)
  {
    if (obj == null || GetType() != obj.GetType())
      return false;
    return this == (Point)obj;
  }

  public override int GetHashCode()
  {
    return x ^ y;
  }

  public override string ToString()
  {
    return "(" + x + ", " + y + ")";
  }

  public static Point operator +(Point lhs, Point rhs)
  {
    return new Point(lhs.x + rhs.x, lhs.y + rhs.y);
  }

  public static Point operator -(Point lhs, Point rhs)
  {
    return new Point(lhs.x - rhs.x, lhs.y - rhs.y);
  }

  public static int Distance(Point lhs, Point rhs)
  {
    return Math.Abs(lhs.x - rhs.x) + Math.Abs(lhs.y - rhs.y);
  }
}
