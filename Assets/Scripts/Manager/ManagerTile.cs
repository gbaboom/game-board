﻿using UnityEngine;

public class ManagerTile : Singleton<ManagerTile>
{
  private int[,] level;

  [SerializeField]
  public Transform holder;

  [SerializeField]
  private GameObject[] prefabs;

  private GameObject[,] tiles;

  public int sizeX { get { return tiles.GetLength(0); } }
  public int sizeY { get { return tiles.GetLength(1); } }

  public void SetLevel(int[,] l)
  {
    level = l;
  }

  public void Load()
  {
    tiles = new GameObject[level.GetLength(0), level.GetLength(1)];
    for (int x = 0; x < tiles.GetLength(0); ++x)
    {
      for (int y = 0; y < tiles.GetLength(1); ++y)
      {
        if (level[x, y] < 0 || level[x, y] >= prefabs.Length)
          level[x, y] = 0;

        GameObject newTile = Instantiate(prefabs[level[x, y]]);
        tiles[x, y] = newTile;
        newTile.GetComponent<TileBase>().Spawn(new Point(x, y));
      }
    }
  }

  public void Unload()
  {
    for (int x = 0; x < tiles.GetLength(0); ++x)
      for (int y = 0; y < tiles.GetLength(1); ++y)
        Destroy(tiles[x, y]);
  }

  public bool IsWalkable(Point position)
  {
    if (position.x < tiles.GetLowerBound(0) || position.x > tiles.GetUpperBound(0) ||
        position.y < tiles.GetLowerBound(1) || position.y > tiles.GetUpperBound(1))
      return false;
    return tiles[position.x, position.y].GetComponent<TileBase>().isWalkable;
  }
}
