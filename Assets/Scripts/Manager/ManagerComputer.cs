﻿public class ManagerComputer : Singleton<ManagerComputer>
{
  private int[,] level;

  public void SetLevel(int[,] l)
  {
    level = l;
  }

  public void Load()
  {
    for (int x = 0; x < level.GetLength(0); ++x)
      for (int y = 0; y < level.GetLength(1); ++y)
        if (level[x, y] != 0)
          ManagerUnit.Instance.Add(new Point(x, y), level[x, y], EnumOwner.Computer);
  }
}
