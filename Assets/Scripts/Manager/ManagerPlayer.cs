﻿using UnityEngine;
using UnityEngine.UI;

public class ManagerPlayer : Singleton<ManagerPlayer>
{
  [SerializeField]
  private int resource;
  private int currentResource;

  public void Load()
  {
    currentResource = resource;
    UpdateUI();
  }

  private void UpdateUI()
  {
    GameObject.Find("UITextResource").GetComponent<Text>().text = "Resource: " + currentResource;
  }

  public void SummonUnit(Point position, int type)
  {
    if (type != 0)
    {
      int cost = ManagerUnit.Instance.GetUnitCost(type);
      // Enough resources and position is not occupied
      if (currentResource >= cost && !ManagerUnit.Instance.IsOccupied(position))
      {
        currentResource -= cost;
        UpdateUI();

        ManagerUnit.Instance.Add(position, type, EnumOwner.Player);
      }
    }
  }
}
