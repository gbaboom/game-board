﻿using UnityEngine;

public class ManagerGame : Singleton<ManagerGame>
{
  [SerializeField]
  private TextAsset LevelTile;

  [SerializeField]
  private TextAsset LevelEnemy;

  [SerializeField]
  private GameObject Camera;

  public enum GameState
  {
    Setup,
    Resolve,
    Cleanup
  };

  public GameState state { get; private set; }

  public void Start()
  {
    ManagerTile.Instance.SetLevel(Utilities.ParseLevel(LevelTile.text));
    ManagerComputer.Instance.SetLevel(Utilities.ParseLevel(LevelEnemy.text));
    Load();
  }

  public void Reload()
  {
    Unload();
    Load();
  }

  private void Load()
  {
    ManagerTile.Instance.Load();
    ManagerUnit.Instance.Load();
    ManagerPlayer.Instance.Load();
    ManagerComputer.Instance.Load();
    ManagerAction.Instance.Load();
    Camera.GetComponent<Camera>().Load();
    ChangeGameState(GameState.Setup);
  }

  private void Unload()
  {
    ManagerTile.Instance.Unload();
    ManagerUnit.Instance.Unload();
  }

  public void ChangeGameState(GameState st)
  {
    state = st;
    ManagerAction.Instance.ChangeGameState(state);
  }
}
