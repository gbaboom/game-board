﻿using UnityEngine;
using UnityEngine.UI;

public class ManagerAction : Singleton<ManagerAction>
{
  [SerializeField]
  private GameObject UIUnits;

  [SerializeField]
  private GameObject buttonPlay;

  private int selectedUnitType;

  public void Load()
  {
    selectedUnitType = 1;
  }

  public void ChangeGameState(ManagerGame.GameState state)
  {
    switch (ManagerGame.Instance.state)
    {
      case ManagerGame.GameState.Setup:
        buttonPlay.transform.FindChild("Text").GetComponent<Text>().text = "Play";
        UIUnits.SetActive(true);
        break;
      case ManagerGame.GameState.Resolve:
        buttonPlay.transform.FindChild("Text").GetComponent<Text>().text = "Pause";
        UIUnits.SetActive(false);
        break;
    }
  }

  public void ButtonUnitPressed(GameObject button)
  {
    if (ManagerGame.Instance.state != ManagerGame.GameState.Setup)
      return;

    selectedUnitType = button.GetComponent<ButtonUnit>().type;
  }

  public void TilePressed(Point position)
  {
    if (ManagerGame.Instance.state != ManagerGame.GameState.Setup)
      return;
    ManagerPlayer.Instance.SummonUnit(position, selectedUnitType);
  }

  public void ButtonPlayPressed()
  {
    switch (ManagerGame.Instance.state)
    {
      case ManagerGame.GameState.Setup:
        ManagerGame.Instance.ChangeGameState(ManagerGame.GameState.Resolve);
        break;
      case ManagerGame.GameState.Resolve:
        ManagerGame.Instance.ChangeGameState(ManagerGame.GameState.Setup);
        break;
    }
  }

  public void ButtonResetPressed()
  {
    ManagerGame.Instance.Reload();
  }

  public void ButtonRandomTilesPressed()
  {
    int[,] level = new int[14, 9];
    for (int x = 0; x < 14; ++x)
    {
      for (int y = 0; y < 9; ++y)
      {
        if (x == 0 || x == 1)
          level[x, y] = 1;
        else if (x == 13 || x == 12)
          level[x, y] = 2;
        else
          level[x, y] = Random.Range(0.0f, 100.0f) < 20.0f ? 0 : 3;
      }
    }

    ManagerTile.Instance.SetLevel(level);
    ManagerGame.Instance.Reload();
  }

  public void ButtonRandomEnemiesPressed()
  {
    int[,] level = new int[14, 9];
    for (int x = 0; x < 14; ++x)
    {
      for (int y = 0; y < 9; ++y)
      {
        if (x < 12)
          level[x, y] = 0;
        else
          level[x, y] = Random.Range(0.0f, 100.0f) < 30.0f ? (int)Random.Range(1.0f, 3.999f) : 0;
      }
    }

    ManagerComputer.Instance.SetLevel(level);
    ManagerGame.Instance.Reload();
  }
}
