﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ManagerUnit : Singleton<ManagerUnit>
{
  [SerializeField]
  public Transform holder;

  [SerializeField]
  private GameObject[] prefabs;

  private Dictionary<EnumOwner, List<GameObject>> units;
  private HashSet<Point> occupied;

  private HashSet<GameObject> setDead;

  private List<UnitMovement> listUnitMovement;
  private List<UnitAttack> listUnitAttack;
  private List<UnitStatus> listUnitStatus;

  public void Load()
  {
    units = new Dictionary<EnumOwner, List<GameObject>>();
    units[EnumOwner.Player] = new List<GameObject>();
    units[EnumOwner.Computer] = new List<GameObject>();
    occupied = new HashSet<Point>();
    setDead = new HashSet<GameObject>();
    listUnitMovement = new List<UnitMovement>();
    listUnitAttack = new List<UnitAttack>();
    listUnitStatus = new List<UnitStatus>();
  }

  public void Unload()
  {
    foreach (var hash in units)
    {
      foreach (var unit in hash.Value)
        Destroy(unit);
      hash.Value.Clear();
    }
    occupied.Clear();
  }

  public void Update()
  {
    if (ManagerGame.Instance.state != ManagerGame.GameState.Resolve)
      return;
    Step();
    RemoveDead();
  }

  private void Step()
  {
    foreach (var elem in listUnitMovement)
      elem.Step();
    foreach (var elem in listUnitAttack)
      elem.Step();
    foreach (var elem in listUnitStatus)
      elem.Step();
  }

  private void RemoveDead()
  {
    foreach (var unit in setDead)
    {
      UnitBase unitBase = unit.GetComponent<UnitBase>();
      units[unitBase.owner].Remove(unit);
      occupied.Remove(unitBase.position);

      UnitMovement unitMovement = unit.GetComponent<UnitMovement>();
      if (unitMovement)
      {
        listUnitMovement.Remove(unitMovement);
      }

      UnitAttack unitAttack = unit.GetComponent<UnitAttack>();
      if (unitAttack)
      {
        listUnitAttack.Remove(unitAttack);
      }

      UnitStatus unitStatus = unit.GetComponent<UnitStatus>();
      if (unitStatus)
      {
        listUnitStatus.Remove(unitStatus);
      }

      Destroy(unit);
    }
    setDead.Clear();
  }

  public void Add(Point position, int type, EnumOwner owner)
  {
    GameObject newUnit = Instantiate(prefabs[type]);
    units[owner].Add(newUnit);
    newUnit.GetComponent<UnitBase>().Load(position, owner);
    newUnit.GetComponent<UnitBase>().Initialize();

    UnitMovement unitMovement = newUnit.GetComponent<UnitMovement>();
    if (unitMovement)
    {
      unitMovement.Initialize();
      listUnitMovement.Add(unitMovement);
    }

    UnitAttack unitAttack = newUnit.GetComponent<UnitAttack>();
    if (unitAttack)
    {
      unitAttack.Initialize();
      listUnitAttack.Add(unitAttack);
    }

    UnitStatus unitStatus = newUnit.GetComponent<UnitStatus>();
    if (unitStatus)
    {
      unitStatus.Initialize();
      listUnitStatus.Add(unitStatus);
    }
  }

  public void Remove(GameObject unit)
  {
    setDead.Add(unit);
  }

  public int GetUnitCost(int type)
  {
    return prefabs[type].GetComponent<UnitBase>().cost;
  }

  public GameObject GetClosest(Point position, EnumOwner owner)
  {
    GameObject result = null;
    int min = int.MaxValue;
    foreach (var gameobject in units[owner])
    {
      int distance = Point.Distance(gameobject.GetComponent<UnitBase>().position, position);
      if (distance != 0 && distance < min)
      {
        result = gameobject;
        min = distance;
      }
    }
    return result;
  }

  public GameObject GetLeastHealth(Point position, EnumOwner owner)
  {
    GameObject result = null;
    float min = float.MaxValue;
    foreach (var gameobject in units[owner])
    {
      float health = gameobject.GetComponent<UnitStatus>().healthPercent;
      if (gameobject.GetComponent<UnitBase>().position != position && health < min)
      {
        result = gameobject;
        min = health;
      }
    }
    return result;
  }

  public IEnumerable<GameObject> GetAllInRange(Point position, EnumOwner owner, int range)
  {
    return units[owner].Where(unit => Point.Distance(unit.GetComponent<UnitBase>().position, position) <= range);
  }

  public void UpdateOccupied(Point oldPos, Point newPos)
  {
    if (oldPos != null)
      occupied.Remove(oldPos);
    occupied.Add(newPos);
  }

  public bool IsOccupied(Point position)
  {
    return occupied.Contains(position);
  }
}
