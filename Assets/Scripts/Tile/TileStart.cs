﻿using UnityEngine;

public class TileStart : MonoBehaviour
{
  private void OnMouseOver()
  {
    if (Input.GetMouseButtonDown(0))
      ManagerAction.Instance.TilePressed(GetComponent<TileBase>().position);
  }
}
