﻿using UnityEngine;
using System.Collections.Generic;

public class TileBase : MonoBehaviour
{
  [SerializeField]
  private bool _isWalkable;
  public bool isWalkable { get { return _isWalkable; } }

  public Point position { get; private set; }

  public void Spawn(Point pos)
  {
    position = pos;
    transform.position = position;
    transform.SetParent(ManagerTile.Instance.holder);
    name = "Tile " + position;
  }
}
